#! /usr/bin/env node

var args = process.argv.slice(2);
var type = args.shift();

type = type ? type : 'web';

var fakelog = require('../lib/fakelog');

if (!fakelog[type]) {
    console.error('Unknown type ' + type);
    process.exit();
}

fakelog[type].run(args);
