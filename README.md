# Fakelog

Sometimes it is nice to see something happen, even when its not.

Why? I asked this same question after I wrote it, answers on a post card please.

## Installation

```bash
$ sudo npm install -g fakelog
```

## Usage

```bash
# Run web fakelog
$ fakelog

# Run bdd fakelog once
$ fakelog bdd

# Run bdd fakelog 11 times
$ fakelog bdd 11

# Run npm fakelog
$ fakelog npm

# Run npm fakelog for 60 seconds
$ fakelog npm 60
```

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
