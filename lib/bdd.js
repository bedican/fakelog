var colors = require('colors');
var sleep = require('sleep');

var features = {
    'As a customer I wish to navigate the site': {
        'The homepage contains a product widget': [
            'Given I am on the homepage',
            'Then the page should contain the text "View Product"'
        ],
        'Able to navigate to a product page from the homepage': [
            'Given I am on the homepage',
            'And I click on the product "52144"',
            'Then I should be taken to the product page for product "52144"',
            'And the page should contain the text "Add to cart"'
        ],
        'Able to navigate to a category listing from the homepage': [
            'Given I am on the homepage',
            'And I click on the category "143"',
            'Then I should be taken to the category page for category "143"',
            'And the page should contain the text "Products for"'
        ]
    }
};

var fakelog = {
    runTests: function() {

        var feature, scenario, steps;
        var scenarioKeys;

        var scenarioCount = 0;
        var stepsCount = 0;

        var featureKeys = Object.keys(features);

        for (fk in featureKeys) {
            feature = featureKeys[fk];
            scenarios = features[feature];

            scenarioCount += Object.keys(scenarios).length;

            console.log('Feature'.bold + ': ' + feature);
            console.log();

            scenarioKeys = Object.keys(scenarios);

            for (sk in scenarioKeys) {
                scenario = scenarioKeys[sk];
                steps = scenarios[scenario];

                stepsCount += steps.length;

                console.log(' Scenario'.bold + ': ' + scenario);

                for(var s in steps) {
                    sleep.sleep(Math.floor(Math.random() * 2) + 1);
                    console.log('   ' + steps[s].replace(/"([^"]+)"/g, '"' + '$1'.bold + '"').green);
                }

                console.log();
            }
        }

        return {
            scenarios: scenarioCount,
            steps: stepsCount
        };
    },
    run: function(args) {

        var repeat = args.shift();

        repeat = repeat ? parseInt(repeat) : 1;

        if (!repeat) {
            console.log('Unknown repetition count');
            return;
        }

        var counts;
        var totalCounts = {
            scenarios: 0,
            steps: 0
        };

        for (var x = 0; x < repeat; x++) {

            counts = this.runTests();

            totalCounts.scenarios += counts.scenarios;
            totalCounts.steps += counts.steps;
        }

        console.log();
        console.log(totalCounts.scenarios + ' scenarios ('+ (totalCounts.scenarios +' passed').green + ')');
        console.log(totalCounts.steps + ' steps (' + (totalCounts.steps + ' passed').green + ')');
        console.log('1m4.02s (17.78Mb)');
    }
};

module.exports = {
    run: function(args) {
        fakelog.run(args);
    }
};
