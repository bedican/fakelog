module.exports = {
    web: require('./web'),
    bdd: require('./bdd'),
    npm: require('./npm')
};
