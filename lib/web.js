var Log = require('log');
var log = new Log('info');

var agents = [
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A',
    'Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16',
    'Mozilla/5.0 (X11; U; Linux i686; fr-fr) AppleWebKit/525.1+ (KHTML, like Gecko, Safari/525.1+) midori/1.19',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246'
];

var requests = [
    '"GET / HTTP/1.1" 200',
    '"GET /products/12588 HTTP/1.1" 200',
    '"GET /products/13419 HTTP/1.1" 200',
    '"GET /products/21552 HTTP/1.1" 200',
    '"GET /css/common.css HTTP/1.1" 304',
    '"GET /js/jquery.min.js HTTP/1.1" 304',
    '"GET /css/bootstrap.min.css HTTP/1.1" 304',
    '"GET /js/bootstrap.min.js HTTP/1.1" 304',
];

var errors = [
    '"GET /favicon.ico HTTP/1.0" 404 4252',
    '"GET /apple-touch-icon.png HTTP/1.0" 404 1330'
];

var fakelog = {
    run: function(args) {

        var _this = this;

        var delay = 1000 * (Math.random() * (3 - 1)) + 1;
        var isError = (Math.floor(Math.random() * 10) > 8);

        if (isError) {
            log.error(errors[Math.floor(Math.random() * errors.length)]);
        } else {
            log.info(requests[Math.floor(Math.random() * requests.length)] + ' - - ' + agents[Math.floor(Math.random() * agents.length)]);
        }

        setTimeout(function() { _this.run(); }, delay);
    }
};

module.exports = {
    run: function(args) {
        fakelog.run(args);
    }
};
