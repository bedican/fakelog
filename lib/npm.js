var ProgressBar = require('progress');
var pad = require('pad');

var spinner = [
    '\u258c',
    '\u2580',
    '\u2590',
    '\u2584'
];

var actions = [
    'loadDep:cloudpirate',
    'loadDep:colors',
    'loadDep:sleep',
    'loadDep:progress',
    'loadDep:lodash',
    'loadDep:grunt-cli',
    'loadDep:gulp',
    'loadDep:express',
    'loadDep:forever', // .. and ever
    'loadDep:chalk',
    'loadDep:request',
    'loadDep:async',
    'loadDep:underscore',
    'loadDep:cloudfish',
    'loadDep:flightplan'
];

var fakelog = {
    run: function(args) {

        var seconds = args.shift();

        seconds = seconds ? parseInt(seconds) : 20;

        if (!seconds) {
            console.log('Invalid seconds argument');
            return;
        }

        // We subtract 28 to make room for the surrounding text.
        var consoleWidth = process.stdout.getWindowSize()[0] - 28;

        // Console width is the seconds duration.
        // We need to work out how much 1 tick is worth in time
        var tick = ((seconds / consoleWidth) * 1000);

        var bar = new ProgressBar(":action :spinner \u2562:bar\u255f", {
            total: consoleWidth,
            incomplete: '\u2591',
            complete: '\u2593'
        });

        var actionIndex = 0;
        var spinnerIndex = 0;

        bar.tick({
            'action': pad(actions[actionIndex], 20, {strip: true}),
            'spinner': spinner[spinnerIndex]
        });

        var actionTimer = setInterval(function() {
            actionIndex++;
            if(actionIndex > (actions.length - 1)) {
                actionIndex = 0;
            }

            spinnerIndex++;
            if(spinnerIndex > (spinner.length - 1)) {
                spinnerIndex = 0;
            }

            bar.tick(0, {
                'action': pad(actions[actionIndex], 20, {strip: true}),
                'spinner': spinner[spinnerIndex]
            });

        }, 100);

        var tickTimer = setInterval(function() {

            bar.tick();

            if (bar.complete) {
                clearInterval(tickTimer);
                clearInterval(actionTimer);
            }

        }, tick);
    }
};

module.exports = {
    run: function(args) {
        fakelog.run(args);
    }
};
